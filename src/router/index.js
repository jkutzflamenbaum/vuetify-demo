import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Contact from '../views/Contact.vue'
import DataPage from '../views/DataPage'
import ActivityForm from '../views/ActivityForm'
import SalonRecord from '../views/salon/SalonRecord'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/data',
    name: 'Data',
    component: DataPage
  },
  {
    path: '/form',
    name: 'ActivityForm',
    component: ActivityForm
  },
  {
    path: '/salonrecord',
    name: 'SalonRecord',
    component: SalonRecord
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: "history",
  routes
})

export default router
