import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    drawerOpen: false
  },
  mutations: {
    TOGGLE_DRAWER(state) { state.drawerOpen = !state.drawerOpen },
    CLOSE_DRAWER(state) { state.drawerOpen = false },
    OPEN_DRAWER(state) { state.drawerOpen = true }
  },
  actions: {
  },
  modules: {
  }
})
